<?php
require_once("../../../vendor/autoload.php");

use \App\Hobbies\Hobbies;

$objHobbies= new Hobbies();

$_POST["hobby"] = implode(",", $_POST["hobby"]);

$objHobbies->setData($_GET);

$objHobbies->trash();