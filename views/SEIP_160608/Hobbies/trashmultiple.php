<?php
require_once("../../../vendor/autoload.php");

use \App\Hobbies\Hobbies;
use App\Message\Message;
use App\Utility\Utility;


if(isset($_POST['mark'])) {

$objHobbies= new Hobbies();


$objHobbies->trashMultiple($_POST['mark']);
    Utility::redirect("trashed.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("index.php");
}