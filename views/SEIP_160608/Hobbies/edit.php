<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objHobbies = new \App\Hobbies\Hobbies();
$objHobbies->setData($_GET);
$oneData = $objHobbies->view();
$hobbyArray= explode(",",$oneData->hobby);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobbies Edit Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

    <div class="navbar">

        <td>
            <a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a>
            <a href='create.php' class='btn btn-group-lg btn-info'>Reload</a>

        </td>

    </div>



    <form  class="form-group f" action="update.php" method="post">

        Please Enter Student's Name:
        <br>
        <input type = "text" name="studentName" value="<?php echo $oneData->name ?>" >
        <br>
        Hobbies: <input type="checkbox" name="hobby[]" value="Travelling" <?php if(in_array("Travelling",$hobbyArray)){echo "checked";} ?>><label>Travelling</label>
        <input type="checkbox" name="hobby[]" value="Reading" <?php if(in_array("Reading",$hobbyArray)){echo "checked";} ?> ><label>Reading</label>
        <input type="checkbox" name="hobby[]" value="Photography" <?php if(in_array("Photography",$hobbyArray)){echo "checked";} ?> ><label>Photography</label><br>
        <br>


        <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

        <input class="btn btn-primary" type="submit" value="Update">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


