<?php



require_once("../../../vendor/autoload.php");
$objProfilePicture = new \App\ProfilePicture\ProfilePicture();


if($_FILES["profilePicture"]["name"]=="")
{

    $objProfilePicture->setData($_POST);
    $oneData =  $objProfilePicture->view();
    $fileName = $oneData->profile_pic;

}
else{
    $fileName = $_FILES["profilePicture"]["name"];

    $source = $_FILES["profilePicture"]["tmp_name"];
    $destination = "UploadedFiles/$fileName";
    move_uploaded_file($source,$destination);

}

$_POST["profilePicture"] = $fileName;
$objProfilePicture->setData($_POST);

$objProfilePicture->update();



?>