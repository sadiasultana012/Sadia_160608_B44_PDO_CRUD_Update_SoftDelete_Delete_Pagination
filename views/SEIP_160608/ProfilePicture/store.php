<?php

require_once("../../../vendor/autoload.php");

$objProfilePicture = new \App\ProfilePicture\ProfilePicture();

$fileName = $_FILES["profilePicture"]["name"];

$source = $_FILES["profilePicture"]["tmp_name"];

$destination = "UploadedFiles/$fileName";

move_uploaded_file($source,$destination);

$_POST["profilePicture"] = $fileName;

$objProfilePicture->setData($_POST);

$objProfilePicture->store();

/*$pictureFile = $_FILES["profilePicture"];

$_POST["profilePicture"]=$pictureFile;



$objProfilePicture->setData($_POST);

$objProfilePicture->store();

$objProfilePicture->moveFile(); */

?>