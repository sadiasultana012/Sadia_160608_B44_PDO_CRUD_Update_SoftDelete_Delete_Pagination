<?php
require_once("../../../vendor/autoload.php");

use App\Message\Message;

if(!isset($_SESSION)){
    session_start();
}
$msg = Message::getMessage();

echo "<div class='container' style='height: 50px'><div id='message'> $msg </div> </div> ";




$objGender = new \App\Gender\Gender();
$objGender->setData($_GET);
$oneData = $objGender->view();
$genderArray= explode(",",$oneData->gender);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gender Edit Form</title>
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../../resource/bootstrap/css/bootstrap-theme.min.css">
    <script src="../../../resource/bootstrap/js/bootstrap.min.js"></script>



</head>
<body>

<div class="container">

    <div class="navbar">

        <td><a href='index.php' class='btn btn-group-lg btn-info'>Active-List</a> </td>

    </div>



    <form  class="form-group f" action="update.php" method="get">

        Enter Name:<br>
        <input class="form-control" type="text" name="studentName" value="<?php echo $oneData->name ?>" >
        <br>
        Enter Gender:
        <input type="radio" name="gender[]" value="Male" <?php if(in_array("Male",$genderArray)){echo "checked";} ?>><label>Male</label>
        <input type="radio" name="gender[]" value="Female" <?php if(in_array("Female",$genderArray)){echo "checked";} ?> ><label>Female</label>
        <br>


        <input type="hidden" name="id" value="<?php echo $oneData->id ?>" >

        <input class="btn btn-primary" type="submit" value="Update">

    </form>

</div>




<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>
    jQuery(function($) {
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
        $('#message').fadeIn (550);
        $('#message').fadeOut (550);
    })
</script>



</body>

</html>


