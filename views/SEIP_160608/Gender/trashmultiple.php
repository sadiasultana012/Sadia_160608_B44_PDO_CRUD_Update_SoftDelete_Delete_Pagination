<?php
require_once("../../../vendor/autoload.php");

use \App\Gender\Gender;
use App\Message\Message;
use App\Utility\Utility;


if(isset($_POST['mark'])) {

$objGender= new Gender();


$objGender->trashMultiple($_POST['mark']);
    Utility::redirect("trashed.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("index.php");
}