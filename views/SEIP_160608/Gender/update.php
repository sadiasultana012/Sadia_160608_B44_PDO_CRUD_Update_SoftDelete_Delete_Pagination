<?php
require_once("../../../vendor/autoload.php");

use \App\Gender\Gender;

$objGender= new Gender();

$_GET["gender"] = implode(",", $_GET["gender"]);

$objGender->setData($_GET);

$objGender->update();