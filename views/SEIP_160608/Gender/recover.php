<?php
require_once("../../../vendor/autoload.php");

use \App\Gender\Gender;

$objGender= new Gender();

$_POST["gender"] = implode(",", $_POST["gender"]);

$objGender->setData($_GET);

$objGender->recover();