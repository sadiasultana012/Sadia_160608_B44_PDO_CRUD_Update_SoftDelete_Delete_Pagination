<?php

namespace App\Summary;

use App\Message\Message;
use App\Utility\Utility;


use App\Model\Database as DB;
use PDO;
use PDOException;

class Summary extends DB
{

       private $id;
       private $organization_name;
       private $summary;



    public function setData($postData){

         if(array_key_exists('id',$postData)){
             $this->id = $postData['id'];
         }

         if(array_key_exists('organization_name',$postData)){
             $this->organization_name = $postData['organization_name'];
         }

         if(array_key_exists('summary',$postData)){
             $this->summary = $postData['summary'];
         }

     }


      public function store(){

          $arrData = array($this->organization_name,$this->summary);

          $sql = "INSERT INTO `summaryoforganization`(`organization_name`, `summary`) VALUES(?,?)";

          $STH = $this->dbh->prepare($sql);

          $result =$STH->execute($arrData);

          if($result)
              Message::message("Success! Data Has Been Inserted Successfully :)");
          else
              Message::message("Failed! Data Has Not Been Inserted :( ");

          Utility::redirect('index.php');


      }



    public function index(){

        $sql = "SELECT * FROM `summaryoforganization` where soft_deleted='No'";

        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "SELECT * FROM `summaryoforganization`  where id=".$this->id;

        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "SELECT * FROM `summaryoforganization` where soft_deleted='Yes'";

        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }




    public function update(){

        $arrData = array($this->organization_name,$this->summary);

        $sql = "UPDATE `summaryoforganization` SET organization_name=?,summary=? WHERE id=".$this->id;

        $STH = $this->dbh->prepare($sql);

        $result =$STH->execute($arrData);

        if($result)
            Message::message("Success! Data Has Been Updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Updated  :( ");

        Utility::redirect('index.php');


    }



    public function trash(){

        $sql = "UPDATE  `summaryoforganization` SET soft_deleted='Yes' WHERE id=".$this->id;

        $result = $this->dbh->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('index.php');


    }


    public function recover(){

        $sql = "UPDATE  `summaryoforganization` SET soft_deleted='No' WHERE id=".$this->id;

        $result = $this->dbh->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php');


    }




    public function delete(){

        $sql = "Delete from `summaryoforganization` WHERE id=".$this->id;

        $result = $this->dbh->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('index.php');


    }


    public function indexPaginator($page=1,$itemsPerPage=3){
      try{

          $start = (($page-1) * $itemsPerPage);
          if($start<0) $start = 0;
          $sql = "SELECT * from `summaryoforganization`  WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";



      }catch (PDOException $error){

          $sql = "SELECT * from `summaryoforganization` WHERE soft_deleted = 'No'";

      }

        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from `summaryoforganization` WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from `summaryoforganization`  WHERE soft_deleted = 'Yes'";

        }

        $STH = $this->dbh->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;




    }






    public function trashMultiple($selectedIDsArray){


            foreach($selectedIDsArray as $id){

                $sql = "UPDATE  `summaryoforganization` SET soft_deleted='Yes' WHERE id=".$id;

                $result = $this->dbh->exec($sql);

                if(!$result) break;

            }



        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }


    public function recoverMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE  `summaryoforganization` SET soft_deleted='No' WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function deleteMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "Delete from `summaryoforganization`  WHERE id=".$id;

            $result = $this->dbh->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from `summaryoforganization` WHERE id=".$id;


            $STH = $this->dbh->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM summaryoforganization` WHERE `soft_deleted` ='No' AND (`organization_name` LIKE '%".$requestArray['search']."%' OR `summary` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `summaryoforganization` WHERE `soft_deleted` ='No' AND `organization_name` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `summaryoforganization` WHERE `soft_deleted` ='No' AND `summary` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->dbh->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->organization_name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->organization_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->summary);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->summary);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords




}