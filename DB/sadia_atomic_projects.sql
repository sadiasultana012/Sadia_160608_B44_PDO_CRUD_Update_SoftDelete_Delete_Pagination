-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 10, 2017 at 07:04 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 5.6.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sadia_atomic_projects`
--

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `birth_date`, `soft_deleted`) VALUES
(1, 'Sadia', '1991-12-11', 'No'),
(2, 'Nadia', '1996-04-05', 'No'),
(3, 'Nowrin', '1999-09-09', 'No'),
(4, 'Noah', '2000-10-10', 'No'),
(5, 'Xyz', '1992-08-07', 'Yes'),
(6, 'Abc', '1998-07-08', 'Yes'),
(7, 'Person1', '2007-12-03', 'No'),
(10, 'Person4', '1989-09-03', 'Yes'),
(11, 'Person5', '2005-12-01', 'Yes'),
(12, 'Person6', '1990-09-29', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE `book_title` (
  `id` int(11) NOT NULL,
  `book_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `author_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'Book1', 'Author1', 'No'),
(2, 'Book3', 'Author3', 'No'),
(3, '', '', 'No'),
(4, 'Book5', 'Author5', 'No'),
(5, 'Book6', 'Author6', 'No'),
(7, 'Book8', 'Author7', 'No'),
(8, 'BookAbc', 'Author8', 'No'),
(9, 'Book9', 'Author9', 'No'),
(10, 'Book10', 'SheAuthor10', 'No'),
(11, 'Book11', 'Author11', 'Yes'),
(12, 'Book12', 'Author12', 'No'),
(13, 'Book13', 'Author13', 'No'),
(14, 'Book14', 'Author14', 'No'),
(15, 'Book15', 'Author15', 'No'),
(16, 'Book16', 'Author16', 'No'),
(17, 'Book17', 'Author17', 'Yes'),
(18, 'Book18', 'Author18', 'No'),
(19, 'Book19', 'Author19', 'No'),
(20, 'Book20', 'Author20', 'No'),
(21, 'Book21', 'Author21', 'Yes'),
(22, 'Book22', 'Author22', 'No'),
(23, 'Book23', 'Author23', 'Yes'),
(24, 'Book24', 'Author24', 'No'),
(25, 'Book25', 'Author25', 'No'),
(26, 'Book26', 'Author26', 'No'),
(27, 'Book27', 'Author27', 'Yes'),
(28, 'Book28', 'Author28', 'Yes'),
(29, 'Book29', 'Author29', 'No'),
(30, 'Book30', 'Author30', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `soft_deleted`) VALUES
(1, 'Dhaka', 'No'),
(2, 'Chittagong', 'No'),
(3, 'Rajshahi', 'No'),
(4, 'Khulna', 'No'),
(6, 'Barisal', 'No'),
(7, 'Sylhet', 'Yes'),
(9, 'Rangpur', 'No'),
(11, 'Gazipur', 'No'),
(12, 'Nawabganj', 'No'),
(24, 'Jessore', 'Yes');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_deleted`) VALUES
(1, 'Sadia', 'sadiasultana012@gmail.com', 'No'),
(2, 'Nadia', 'nadia@yahoo.com', 'No'),
(3, 'Nowrin', 'nowrin@gmail.com', 'No'),
(4, 'Person1', 'person1@yahoo.com', 'No'),
(5, 'Person2', 'person2@gmail.com', 'Yes'),
(7, 'Person3', 'mymail@me.com', 'Yes'),
(8, 'Person4', 'mailperson4@pfour.com', 'Yes'),
(9, 'Person5', 'mailperson5@pfive.com', 'Yes'),
(11, 'Person7', 'mailperson7@psevven.com', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `organization_summary`
--

CREATE TABLE `organization_summary` (
  `id` int(11) NOT NULL,
  `org_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `owner_name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `founding_time` date NOT NULL,
  `mission` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organization_summary`
--

INSERT INTO `organization_summary` (`id`, `org_name`, `type`, `owner_name`, `founding_time`, `mission`, `soft_deleted`) VALUES
(1, 'Org. A', 'Small Business', 'Mr. A', '2012-01-27', 'Spread Handicrafts', 'No'),
(2, 'Org. B', 'NGO', 'Mr. B', '2015-11-01', 'Alleviate Poverty', 'No'),
(3, 'Org. C', 'Educational Institution', 'Miss C', '2012-12-12', 'Educating People', 'No'),
(4, 'Org. D', 'Software Development Firm', 'Miss D', '2016-12-02', 'Prepare Excellent Softwares for diverse applications', 'No'),
(5, 'Org. F', 'Cultural', 'Mr. F', '2016-11-02', 'Practice Cultural Activities', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `picture`
--

CREATE TABLE `picture` (
  `id` int(11) NOT NULL,
  `picture` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE `profile_picture` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `pic` longtext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `profile_pic`, `soft_deleted`, `pic`) VALUES
(31, 'p1', '1-Blue_Fairy.jpg', 'No', 'Array'),
(32, 'p2', 'cute (5).jpg', 'No', 'Array'),
(33, 'p3', 'propic1.jpg', 'No', 'Array'),
(34, 'p4', 'propic2.jpg', 'No', 'Array'),
(35, 'p5', 'propic5.jpg', 'Yes', 'Array'),
(36, 'p7', 'propic3.jpg', 'No', 'Array');

-- --------------------------------------------------------

--
-- Table structure for table `students_genders`
--

CREATE TABLE `students_genders` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `gender` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students_genders`
--

INSERT INTO `students_genders` (`id`, `name`, `gender`, `soft_deleted`) VALUES
(1, 'Sadia', 'Female', 'No'),
(2, 'Nadia', 'Female', 'No'),
(3, 'Nowrin', 'Female', 'No'),
(4, 'Noah', 'Male', 'No'),
(5, 'Tasmiah', 'Female', 'No'),
(6, 'Arik', 'Male', 'No'),
(8, 'Student2', 'Male', 'Yes'),
(9, 'Student3', 'Female', 'Yes'),
(13, 'Miss X', 'Female', 'Yes'),
(14, 'Student3', 'Female', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `students_hobbies`
--

CREATE TABLE `students_hobbies` (
  `id` int(11) NOT NULL,
  `name` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `hobby` varchar(111) COLLATE utf8_unicode_ci NOT NULL,
  `soft_deleted` varchar(111) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `students_hobbies`
--

INSERT INTO `students_hobbies` (`id`, `name`, `hobby`, `soft_deleted`) VALUES
(1, 'Sadia', 'Travelling', 'No'),
(2, 'Nadia', 'Reading', 'No'),
(3, 'Nowrin', 'Photography', 'No'),
(5, 'Noah', 'Reading', 'No'),
(13, 'Jannah', 'Travelling', 'No'),
(14, 'Tasmiah', 'Photography', 'No'),
(16, 'Tasmiah', 'Reading', 'No'),
(17, 'Student1', 'Reading', 'Yes'),
(18, 'Student2', 'Travelling,Reading', 'Yes'),
(19, 'Student3', 'Travelling,Reading,Photography', 'Yes'),
(21, 'SSD', 'Travelling,Reading,Photography', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organization_summary`
--
ALTER TABLE `organization_summary`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_genders`
--
ALTER TABLE `students_genders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_hobbies`
--
ALTER TABLE `students_hobbies`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `organization_summary`
--
ALTER TABLE `organization_summary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `picture`
--
ALTER TABLE `picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `students_genders`
--
ALTER TABLE `students_genders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `students_hobbies`
--
ALTER TABLE `students_hobbies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
